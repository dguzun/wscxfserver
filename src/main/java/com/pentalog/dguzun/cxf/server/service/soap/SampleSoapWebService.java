package com.pentalog.dguzun.cxf.server.service.soap;

import javax.jws.WebService;

import com.pentalog.dguzun.cxf.server.vo.PersonVO;

@WebService
public interface SampleSoapWebService {
	public PersonVO getDataFromWebService();
}
