package com.pentalog.dguzun.cxf.server.service.soap.impl;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Path;

import com.pentalog.dguzun.cxf.server.service.soap.SampleSoapWebService;
import com.pentalog.dguzun.cxf.server.vo.PersonVO;

@WebService(endpointInterface = "com.pentalog.dguzun.cxf.server.service.soap.SampleSoapWebService")
@Path("/persons")
public class SampleSoapWebServiceImpl implements SampleSoapWebService {

	@Override
	@WebMethod
	public PersonVO getDataFromWebService() {
		PersonVO person = new PersonVO();
		person.setName("Thomas Edison");
		person.setAge(78);
		person.setIsAdmin(true);
		return person;
	}
}