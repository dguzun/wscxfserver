package com.pentalog.dguzun.cxf.server.service.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.pentalog.dguzun.cxf.server.vo.PersonVO;

@Path("/persons")
public class SampleRestWebService {

	@GET
	//@Produces({"application/xml"})
	@Produces({"application/json"})
	public Response getRestDataFromWebService() {
		PersonVO person = new PersonVO();
		person.setName("Thomas Edison");
		person.setAge(78);
		person.setIsAdmin(true);

        /*if(a == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }else{*/
        return Response.ok(person).build();
        //}
	}
	
	@POST
	@Produces({"application/xml","application/json"})
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	public Response postRestDataFromWebService() {
		PersonVO person = new PersonVO();
		person.setName("Thomas Edison");
		person.setAge(78);
		person.setIsAdmin(true);

        /*if(a == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }else{*/
        return Response.ok(person).build();
        //}
	}
}